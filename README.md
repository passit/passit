# Passit

This is a meta repo for general issue tracking, wiki, and deployment information.

- [Django Backend](https://gitlab.com/passit/passit-backend)
- [Angular Frontend](https://gitlab.com/passit/passit-frontend)
- [Documentation](https://passit.io/documentation/)

# Installing Passit on DigitalOcean App Platform

Passit on DO App Platform consists of a web, migration job, and postgres database. Start by clicking the following button. Note this link acts as a referral and helps fund Passit.

[![Deploy to DO](https://www.deploytodo.com/do-btn-blue.svg)](https://cloud.digitalocean.com/apps/new?repo=https://gitlab.com/passit/passit/tree/master&refcode=7e90b8fb37f8)

Leave environment variables blank and click next. Pick the basic or pro plan. One 512 MB RAM | 1 vCPU is fine to start with. Click Launch. Now copy [app-platform.yaml](https://gitlab.com/passit/passit/-/blob/master/app-platform.yaml) to your local computer. Edit the following

## Environment Variables

The environment variables you will need to set can be found in the "Required settings" section of our install [docs](https://passit.io/install/#required-settings).

## Name and region

This can be anything. We default to "passit" and "nyc".

## Deploying

You'll need to install [doctl](https://www.digitalocean.com/docs/apis-clis/doctl/how-to/install/) and log in. 

Run `doctl apps list` to get your app's id.

Now apply your app-platform.yaml spec with `doctl apps update 11111111-1111-1111-1111-111111111 --spec app-platform.yaml` (enter your actual id)

After deployment, you should be able to visit the app URL and start using Passit!

## Production considerations

If you intend to use Passit in production, consider upgrading your Postgres database to a production instance. In the web interface, go to Manage Components, passit-db, Upgrade to a managed database.

If you haven't already, you'll need to set up email via environment variables. 

## Upgrading Passit

By default, the docker image tag is "latest". Click Deploy to upgrade to the latest Passit docker image.
